import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AdminDashboardComponent } from './dashboard/admin-dashboard/admin-dashboard.component';
import { AuthPageComponent } from './auth-page/auth-page.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NewProjectComponent } from './dashboard/new-project/new-project.component';
import { DevelopersComponent } from './dashboard/developers/developers.component';
import { DevDashboardComponent } from './dashboard/dev-dashboard/dev-dashboard.component';
import { ProjectViewComponent } from './dashboard/project-view/project-view.component';
import { AppRoutingModule } from './app-routing.module';
import { AlertComponent } from './common-components/alert/alert.component';
import { AlertService } from './shared/alert.service';
import { ProjectService } from './shared/project.service';
import { StatusPipes } from './shared/status.pipes';
import { ProjectLogicService } from './shared/project-logic.service';
import { DeveloperService } from './shared/developer.service';
import { ValidationService } from './shared/validation.service';
import { MyDatePickerModule } from 'mydatepicker';
import { AuthService } from './auth-page/auth.service';

@NgModule({
  declarations: [
    AppComponent,
    AdminDashboardComponent,
    AuthPageComponent,
    DashboardComponent,
    NewProjectComponent,
    DevelopersComponent,
    DevDashboardComponent,
    ProjectViewComponent,
    AlertComponent,
    StatusPipes
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MyDatePickerModule
  ],
  providers: [
    AlertService,
    ProjectService,
    ProjectLogicService,
    DeveloperService,
    ValidationService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
