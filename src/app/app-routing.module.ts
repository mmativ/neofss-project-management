import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthPageComponent } from './auth-page/auth-page.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NewProjectComponent } from './dashboard/new-project/new-project.component';
import { AdminDashboardComponent } from './dashboard/admin-dashboard/admin-dashboard.component';
import { DevelopersComponent } from './dashboard/developers/developers.component';
import { ProjectViewComponent } from './dashboard/project-view/project-view.component';
import { DevDashboardComponent } from './dashboard/dev-dashboard/dev-dashboard.component';

const appRoutes: Routes = [
    { path: '', redirectTo: '/auth', pathMatch: 'full' },
    { path: 'auth', component: AuthPageComponent },
    { path: 'dashboard', component: DashboardComponent, children: [
        { path: '' , component: AdminDashboardComponent},
        { path: 'newproject' , component: NewProjectComponent},
        { path: 'developers' , component: DevelopersComponent},
        { path: 'developers/:id' , component: DevDashboardComponent},
        { path: 'developers/:id/edit' , component: DevelopersComponent},
        { path: 'project/:id' , component: ProjectViewComponent},
        { path: 'project/:id/edit' , component: NewProjectComponent},
        {path: '**', redirectTo: '/dashboard'}
    ]},
    {path: '**', redirectTo: '/auth'}
]; 
 
@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})

export class AppRoutingModule{

}