import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from './auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService } from '../shared/alert.service';
import { AlertModel } from '../shared/alert.model';

@Component({
  selector: 'auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss']
})
export class AuthPageComponent implements OnInit, OnDestroy {
  signinForm: any;
  signinListener: any;

  constructor(
    private authService: AuthService,
    private router: Router,
    private alert: AlertService
  ) { }

  ngOnInit() {
    this.signinForm = new FormGroup({
      'email': new FormControl(null, Validators.required),
      'password': new FormControl(null, Validators.required)
    });
    this.signinListener = this.authService.isSignin.subscribe(
      (result: boolean) => {
        if(!result){
          this.alert.onAlertCalled.next(new AlertModel(true,"Alert...", "Email address or password is incorrect..", true));
        }else{
          this.router.navigate(['/dashboard']);
        }
      }
    );
  }
  onSignin(){
    const _email = this.signinForm.get('email').value;
    const _password = this.signinForm.get('password').value;
    const signIn = this.authService.signinUser(_email, _password);
  }
  
  ngOnDestroy(){
    this.signinListener.unsubscribe();
  }

}
