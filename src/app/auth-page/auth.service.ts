import * as firebase from 'firebase';
import { Subject } from 'rxjs/Subject';

export class AuthService{
    token: string;
    isSignin = new Subject<boolean>();
    constructor(){}
    signupUser(email: string, password: string){
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .catch(
            error => console.log(error)
        );
    }
    signinUser(email: string, password: string){
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(
            (response) => {
                firebase.auth().currentUser.getIdToken()
                .then(
                    (tk: string) => {
                        this.token = tk;
                    }
                );
                this.isSignin.next(true);
                console.log(response);
            }
        )
        .catch(
            (error) => {
                this.isSignin.next(false);
            }
        );
    }
    getToken(){
        firebase.auth().currentUser.getIdToken()
            .then(
                (tk: string) => {
                    this.token = tk;
                }
            );
        return this.token;
    }
}