import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app works!';
  
  ngOnInit(){
    firebase.initializeApp({
      apiKey: "AIzaSyDbpMXyV05N4ixSJa0UNv2Pdmlb3jaGh5w",
      authDomain: "dev-project-management.firebaseapp.com"
    });
  }
}
