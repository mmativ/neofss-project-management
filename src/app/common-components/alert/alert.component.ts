import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { AlertModel } from '../../shared/alert.model';
import { AlertService } from '../../shared/alert.service';
import { DeveloperService } from '../../shared/developer.service';
import { DeveloperModel } from '../../shared/developers.model';
import { ProjectService } from '../../shared/project.service';
import { ProjectModel } from '../../shared/project.model';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit, OnDestroy {
  private alertStatus: any;
  private alertResult: AlertModel;
  private isAlertOpen: boolean = false;
  private alertListener: any;

  constructor(
    private alertSerive: AlertService,
    private devService: DeveloperService,
    private projects: ProjectService
  ) {}
  ngOnInit() {
    this.alertListener = this.alertSerive.onAlertCalled.subscribe(
      (alertResult: AlertModel) =>{
        this.alertStatus = alertResult;
        this.isAlertOpen = this.alertStatus.status;
      }
    );
  }
  onCancel(){
    this.isAlertOpen = false;
  }
  onAccept(){
    if(this.alertStatus.operationtype === 1){
      if(this.alertStatus.operationsubject === 0){
        this.devService.onAddDeveloper(new DeveloperModel(
          this.getUniqueIDs(),
          this.alertStatus.object.devname,
          this.alertStatus.object.devposition,
          this.alertStatus.object.devemail,
          this.alertStatus.object.devcontact,
          this.alertStatus.object.devpwd
        ));
        this.alertSerive.onALertResponse.next(true);
      }else if(this.alertStatus.operationsubject === 1){
        const startDate = this.onCombineDate(
          this.alertStatus.object.projectstart.date.year,
          this.alertStatus.object.projectstart.date.month,
          this.alertStatus.object.projectstart.date.day);
        const deadlineDate = this.onCombineDate(
          this.alertStatus.object.projectdeadline.date.year,
          this.alertStatus.object.projectdeadline.date.month,
          this.alertStatus.object.projectdeadline.date.day);
        this.projects.onAddProject(new ProjectModel(
          this.getUniqueIDs(),
          this.alertStatus.object.projectname,
          this.alertStatus.object.projectdesc,
          startDate,
          deadlineDate,
          this.alertStatus.object.projectbonus,
          this.alertStatus.object.developers,
        ));
        this.alertSerive.onALertResponse.next(true);
      }
      console.log('do save method');
    }else if(this.alertStatus.operationtype === 2){
      if(this.alertStatus.operationsubject == 0){
        this.devService.onUpdateDeveloper(this.alertStatus.object);
        this.alertSerive.onALertResponse.next(true);
      }else if(this.alertStatus.operationsubject == 1){
        this.projects.onUpdateProject(this.alertStatus.object);
        this.alertSerive.onALertResponse.next(true);
      }
      console.log('do update method');
    }else if(this.alertStatus.operationtype === 3){
      if(this.alertStatus.operationsubject == 0){
      
      }else if(this.alertStatus.operationsubject == 1){
        this.projects.onDeleteProject(this.alertStatus.object);
        this.alertSerive.onALertResponse.next(true);
      }
      console.log('do delete method');
    }
    this.isAlertOpen = false;
  }
  getUniqueIDs() {
    let firstPart: any = (Math.random() * 46656) | 0;
    let secondPart: any = (Math.random() * 46656) | 0;
    firstPart = ('000' + firstPart.toString(36)).slice(-3);
    secondPart = ('000' + secondPart.toString(36)).slice(-3);
    return firstPart + secondPart;
  }
  ngOnDestroy(){
    this.alertListener.unsubscribe();
  }
  onCombineDate(_year, _month, _day){
    return _year + '/' + _month + '/' + _day;
  }
}
