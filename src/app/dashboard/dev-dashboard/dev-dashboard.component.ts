import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';
import { DeveloperService } from '../../shared/developer.service';
import { DeveloperModel } from '../../shared/developers.model';
import { ProjectService } from '../../shared/project.service';
import { ProjectLogicService } from '../../shared/project-logic.service';
import { ProjectLogicModel } from '../../shared/project-logic.model';

@Component({
  selector: 'dev-dashboard',
  templateUrl: './dev-dashboard.component.html',
  styleUrls: ['./dev-dashboard.component.scss']
})
export class DevDashboardComponent implements OnInit, OnDestroy {
  private devIdParams: any;
  private devId: any;
  private devDatas: any;
  public devProjects: ProjectLogicModel[] = [];
  filter: any = '6';
  searchValue: string = null;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private developers: DeveloperService,
    private location: Location,
    private projects: ProjectService,
    private projectLogic: ProjectLogicService
    ) { 
      this.devProjects = [];
    }

  ngOnInit() {
    this.devIdParams = this.route.params.subscribe(params => {
       this.devId = params['id'];
    });
    this.devDatas = this.developers.onGetSingleDevelopers(this.devId);
    this.devProjects = this.projectLogic.onDevGetListOfProjects(this.devId);
  }
  ngOnDestroy() {
    this.devIdParams.unsubscribe();
  }
  onBack(): void{
    this.location.back();
  }
  filterChange(_value){
    if (_value >= 6) {
      this.devProjects = this.projectLogic.onDevGetListOfProjects(this.devId);
    }else {
      const projectFiltered = this.projectLogic.onGetProjectFiltered(+_value);
      this.devProjects = projectFiltered;
    }
  }
  onSearch(toSearch){
    if(toSearch.length <= 0) {
      this.devProjects = this.projectLogic.onDevGetListOfProjects(this.devId);
    }else{
      const results = [];
      toSearch = trimString(toSearch).toLowerCase(); // trim it
      for(let i = 0; i < this.devProjects.length; i++) {
        for(let key in this.devProjects[i]) {
          if(this.devProjects[i].projectname.toLowerCase().indexOf(toSearch)!=-1) {
            if(!itemExists(results, this.devProjects[i])) {
              results.push(this.devProjects[i]);
            }
          }
        }
      }
      this.devProjects = results;
    }
    function trimString(s) {
      let l=0, r=s.length -1;
      while(l < s.length && s[l] == ' ') l++;
      while(r > l && s[r] == ' ') r-=1;
      return s.substring(l, r+1);
    }
    function compareObjects(o1, o2) {
      let k = '';
      for(k in o1){
        if(o1[k] != o2[k]){
          return false;
        }
      } 
      for(k in o2){
        if(o1[k] != o2[k]) return false;
      } 
      return true;
    }
    function itemExists(haystack, needle) {
      for(let i=0; i<haystack.length; i++){
        if(compareObjects(haystack[i], needle)) return true;
      } 
      return false;
    }
  }
}
