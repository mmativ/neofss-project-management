import { Component, OnInit } from '@angular/core';
import { ProjectLogicModel } from '../../shared/project-logic.model';
import { ProjectLogicService } from '../../shared/project-logic.service';
import { DeveloperService } from '../../shared/developer.service';
import { AuthService } from '../../auth-page/auth.service';

@Component({
  selector: 'admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {
  projectLists: ProjectLogicModel[];
  private isFiltered: boolean = false;

  constructor(
    private projects: ProjectLogicService,
    private developer: DeveloperService,
    private auth: AuthService
  ) { }

  ngOnInit() {
    this.projectLists = this.projects.onGetProjectDatas();
    console.log(this.auth.getToken());
  }
  getDeveloperDetails(devId){
    const devDetails = this.developer.onGetSingleDevelopers(devId);
    return devDetails;
  }
  getCountOfProject(statusNum){
    const projectCount = this.projects.getStatusCount(statusNum);
    return projectCount;
  }
  onfilterProjects(value){
    this.isFiltered = true;
    const projectFiltered = this.projects.onGetProjectFiltered(value);
    this.projectLists = projectFiltered;
  }
  onClearFilter(){
    this.isFiltered = false;
    this.projectLists = this.projects.onGetProjectDatas();
  }

}
