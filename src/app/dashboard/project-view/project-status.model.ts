export class ProjectStatusModel{
    constructor(
        public projectId: string = null,
        public projectEndDate: string = null,
        public projectStatus: number = null
    ){
    }
}