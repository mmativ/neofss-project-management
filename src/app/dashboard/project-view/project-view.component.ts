import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, FormArray , Validators } from '@angular/forms';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Observable';

import { AlertService } from '../../shared/alert.service';
import { AlertModel } from '../../shared/alert.model';
import { ProjectLogicService } from '../../shared/project-logic.service';
import { ProjectLogicModel } from '../../shared/project-logic.model';
import { DeveloperService } from '../../shared/developer.service';
import {IMyDpOptions} from 'mydatepicker';
import { ProjectStatusModel } from './project-status.model';
import { ProjectModel } from '../../shared/project.model';
import { ProjectService } from '../../shared/project.service';

@Component({
  selector: 'project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.scss']
})
export class ProjectViewComponent implements OnInit, OnDestroy {
  responseListener: any;
  projectStatusObject: ProjectModel;
  projectViewForm: FormGroup;
  private projectidParams: any;
  private projectid: any;
  private projectDetails: any;
  thisYear: any;
  thisMonth: any;
  thisDay: any;
  private projectStartOption: IMyDpOptions = {
      dateFormat: 'yyyy-mm-dd',
      disableWeekends: true
  };
  private projectFinishDate: object;

  constructor(
    private route: ActivatedRoute,
    private alertService: AlertService,
    private projectdata: ProjectLogicService,
    private project: ProjectService,
    private developer: DeveloperService,
    private router: Router,
    private location: Location) { 
      const date = new Date();
      const currentMonth = date.getMonth() + 1;
      const currentYear = date.getFullYear();
      const currentDay = date.getDate();
      this.thisYear = currentYear;
      this.thisMonth = currentMonth;
      this.thisDay = currentDay;
      this.projectFinishDate = { date: { year: this.thisYear, month: this.thisMonth, day: this.thisDay } };
    }

  ngOnInit() {
    this.responseListener = this.alertService.onALertResponse.subscribe(
      (alertResponse: boolean) =>{
        console.log('re route');
        this.router.navigate(['/dashboard']);
      }
    );
    this.projectidParams = this.route.params.subscribe(params => {
       this.projectid = params['id'];
    });
    this.projectDetails = this.projectdata.onGetProjectDetails(this.projectid);
    this.projectStatusObject = this.project.getSingleProject(this.projectid);
    this.projectViewForm = new FormGroup({
      'projectenddate': new FormControl(null, Validators.required, this.projectEndValidation.bind(this)),
      'statusvalue': new FormControl(0,[Validators.required, this.projectStatusValue])
    });
  }
  onBack(): void{
    this.location.back();
  }
  onDelete(){
    const alertModel = new AlertModel(
      true,
      'Confirm...',
      'Are you sure you want to delete this project?',
      false,
      3,
      1,
      this.projectid
    );
    this.alertService.onAlertCalled.next(alertModel);
  }
  getDeveloperDetails(devId){
    const devDetails = this.developer.onGetSingleDevelopers(devId);
    return devDetails;
  }
  ngOnDestroy() {
    this.projectidParams.unsubscribe();
    this.responseListener.unsubscribe();
  }
  projectEndValidation(control: FormControl): Promise<any> | Observable<any> {
    const fromDate = new Date(this.projectDetails[0].projectstart);
    const promise = new Promise<any>((resolve, reject)=>{
      setTimeout(()=>{
        const newDate = new Date(control.value.date.year + '-' + control.value.date.month + '-' + control.value.date.day);
        if(newDate < fromDate) {
          resolve({'endDateIsInvalid': true});
        }else{
          resolve(null);
        }
      }, 400);
    });
    return promise;
  }
  projectStatusValue(control: FormControl): {[s: string]: boolean} {
    if (control.value === 0) {
        return { 'wrongstatus': true };
    }
    return null;
  }
  onSubmitForm(){
    if (this.projectViewForm.valid) {
      const statusEnd = this.projectViewForm.value.projectenddate.date.year
      + '/' + this.projectViewForm.value.projectenddate.date.month
      + '/' + this.projectViewForm.value.projectenddate.date.day;
      const statusValue = this.projectViewForm.value.statusvalue;
      this.projectStatusObject.projectend = statusEnd;
      this.projectStatusObject.projectstate = statusValue;
      const alertData = new AlertModel(
        true,
        'Confirm',
        'Are you sure you want to update this project',
        false,
        2,
        1,
        this.projectStatusObject
      );
      this.alertService.onAlertCalled.next(alertData);
    }else{
      this.alertService.onAlertCalled.next(new AlertModel(true,'Alert...','Please Check the Form...', true));
    }
  }
}
