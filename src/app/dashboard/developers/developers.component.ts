import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { DeveloperService } from '../../shared/developer.service';
import { DeveloperModel } from '../../shared/developers.model';
import { AlertService } from '../../shared/alert.service';
import { AlertModel } from '../../shared/alert.model';

@Component({
  selector: 'app-developers',
  templateUrl: './developers.component.html',
  styleUrls: ['./developers.component.scss']
})
export class DevelopersComponent implements OnInit, OnDestroy {
  private devDatas: DeveloperModel[];
  private devForm: FormGroup;
  private isSuccess: boolean;
  private responseListener: any;
  private subscription: any;
  devIdSubscription: any;
  devIdValue: any;
  paramsSubcriptions: any;
  paramsValue: boolean;
  onEditDivData: DeveloperModel;

  constructor(
    private developer: DeveloperService,
    private confirm: AlertService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit() {
    this.responseListener = this.confirm.onALertResponse.subscribe(
      (alertResponse: boolean) =>{
        this.isSuccess = alertResponse;
        if(this.paramsValue){
          this.router.navigate(['/dashboard/developers', this.devIdValue]);
        }else{
          this.onFormCLear();
        }
      }
    );
    this.devIdSubscription = this.route.params.subscribe(params => {
       this.devIdValue = params['id'];
    });
    this.paramsSubcriptions = this.route.queryParams.subscribe(
      (params: any) =>{
        this.paramsValue = params['allowEdit'];
      }
    );
    this.devDatas = this.developer.onGetDevelopers();
    this.subscription = this.developer.devDatasSync.subscribe(
      (devdatas: DeveloperModel[]) => {
        this.devDatas = devdatas;
    });
    this.devForm = new FormGroup({
      'devname': new FormControl(null, [Validators.required, Validators.minLength(4)]),
      'devposition': new FormControl(null, Validators.required),
      'devemail': new FormControl(null, [Validators.required, this.onValidateEmail]),
      'devcontact': new FormControl(null,[Validators.required,Validators.minLength(6)]),
      'authpwd': new FormGroup({
        'devpwd': new FormControl(null,[Validators.required,Validators.minLength(6), this.onPasswordStrength]),
        'devconfirmpwd': new FormControl(null,[Validators.required,Validators.minLength(6)])
      }, passwordMatchValidator)
    });
    function passwordMatchValidator(g: FormGroup) {
      return g.get('devpwd').value === g.get('devconfirmpwd').value
        ? null : {'notmatch': true};
    }
    if (this.paramsValue) {
      this.onEditDivData = this.developer.onGetSingleDevelopers(this.devIdValue);
      this.devForm.patchValue({
        'devname': this.onEditDivData.devname,
        'devposition': this.onEditDivData.devposition,
        'devemail': this.onEditDivData.devemail,
        'devcontact': this.onEditDivData.devcontact
      });
      this.devForm.get('authpwd').patchValue({
        'devpwd': this.onEditDivData.devpwd,
        'devconfirmpwd': this.onEditDivData.devpwd
      });
    }
  }
  onSaveDevelopers(){
    if(this.devForm.valid){
      if (this.paramsValue) {
        if(this.devForm.touched){
          const devObject: DeveloperModel = new DeveloperModel(
            this.devIdValue,
            this.devForm.get('devname').value,
            this.devForm.get('devposition').value,
            this.devForm.get('devemail').value,
            this.devForm.get('devcontact').value,
            this.devForm.get('authpwd.devpwd').value,
          );
          const confirmVal = new AlertModel(
            true,
            'Confirm...',
            'Are you sure you want to update ' + this.onEditDivData.devname + ' data?',
            false,
            2,
            0,
            devObject
          );
          this.confirm.onAlertCalled.next(confirmVal);
        }else{
          this.confirm.onAlertCalled.next(new AlertModel(true,"Alert...", "You didn't touched the form...", true));
        }
      }else {
        const confirmVal = new AlertModel(
          true,
          'Confirm...',
          'Are you sure you want to save this data?',
          false,
          1,
          0,
          this.devForm.value
        );
        this.confirm.onAlertCalled.next(confirmVal);
      }
    }else{
      this.confirm.onAlertCalled.next(new AlertModel(true,'Alert...','Please Check the Form...', true));
    }
  }
  onValidateEmail(control: FormControl): {[s: string]: boolean} {
    const EMAIL_REGEXP = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    if (!EMAIL_REGEXP.test(control.value)) {
        return { 'incorrectMailFormat': true };
    }
    return null;
  }
  onPasswordStrength(control: FormControl): {[s: string]: boolean} {
    const PW_REGEXP = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$/;
    if (!PW_REGEXP.test(control.value)) {
        return { 'pwstrength': true };
    }
    return null;
  }
  ngOnDestroy(){
    this.responseListener.unsubscribe();
    this.subscription.unsubscribe();
    this.paramsSubcriptions.unsubscribe();
  }
  onFormCLear(){
    this.devForm.reset();
  }
}
