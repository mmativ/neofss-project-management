import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, FormArray , Validators } from '@angular/forms';
import { DeveloperService } from '../../shared/developer.service';
import { DeveloperModel } from '../../shared/developers.model';
import {IMyDpOptions} from 'mydatepicker';
import { Observable } from 'rxjs/Observable';
import { AlertModel } from '../../shared/alert.model';
import { AlertService } from '../../shared/alert.service';
import { ProjectModel } from '../../shared/project.model';
import { ProjectService } from '../../shared/project.service';
import { DevCheckboxModel } from './developer-checkbox.model';


@Component({
  selector: 'new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.scss']
})
export class NewProjectComponent implements OnInit, OnDestroy {
  private projectObject: any;
  private developerArray: any = [];
  private projectForm: FormGroup;
  private developers: DeveloperModel[];
  isDeveloperChecked: DevCheckboxModel[] = []
  private responseListener: any;
  private isSuccess: boolean;
  thisYear: any;
  thisMonth: any;
  thisDay: any;
  projectIdSubscription: any;
  projectIdValue: any = null;
  paramsSubscription: any;
  paramsValue: boolean = null;
  projectData: ProjectModel;
  updatePorjectStartDate: any;
  updatePorjectEndDate: any;
  private projectStartOption: IMyDpOptions = {
        dateFormat: 'yyyy-mm-dd',
        disableWeekends: true
    };
  private startDate: object;
  private endDate: object;

  constructor(
    private developer: DeveloperService,
    private confirm: AlertService,
    private route: ActivatedRoute,
    private project: ProjectService,
    private router: Router
    ) {
    const date = new Date();
    const currentMonth = date.getMonth() + 1;
    const currentYear = date.getFullYear();
    const currentDay = date.getDate();
    this.thisYear = currentYear;
    this.thisMonth = currentMonth;
    this.thisDay = currentDay;
  }

  ngOnInit() {
    this.projectIdSubscription = this.route.params.subscribe(
      (params: any) => {
        this.projectIdValue = params['id'];
      }
    );
    this.paramsSubscription = this.route.queryParams.subscribe(
      (params: any) => {
        this.paramsValue = params['allowEdit'];
      }
    );
    this.responseListener = this.confirm.onALertResponse.subscribe(
      (alertResponse: boolean) =>{
        this.isSuccess = alertResponse;
        if(this.paramsValue){
          this.router.navigate(['/dashboard/project', this.projectIdValue]);
        }else{
          this.onFormCLear();
        }
      }
    );
    this.projectData = this.project.getSingleProject(this.projectIdValue);
    this.paramsValue ? this.onSetPorjectDate(true) : this.onSetPorjectDate(false);
    this.developers = this.developer.onGetDevelopers();
    this.onBindingCheckbox();
    this.projectForm = new FormGroup({
      'projectname': new FormControl(null,[Validators.required, Validators.minLength(4)]),
      'projectdesc': new FormControl(null,[Validators.required, Validators.minLength(10)]),
      'projectstart': new FormControl('', Validators.required, this.projectStartValidation.bind(this)),
      'projectdeadline': new FormControl('', Validators.required, this.projectEndValidation.bind(this)),
      'projectbonus': new FormControl(null,[Validators.required, Validators.minLength(1)])
    });
    if (this.paramsValue){
      this.projectForm.patchValue({
        'projectname': this.projectData.projectname,
        'projectdesc': this.projectData.projectdesc,
        'projectstart': this.projectData.projectstart,
        'projectbonus': this.projectData.projectbonus,
      });
    }
  }
  ngOnDestroy(){
    this.projectIdSubscription.unsubscribe();
    this.paramsSubscription.unsubscribe();
    this.responseListener.unsubscribe();
  }
  // getting checkbox value ------------------
  selectedOptions() {
    return this.isDeveloperChecked
              .filter(opt => opt.checked)
              .map(opt => opt.value)
  }
  // getting checkbox value ------------------
  onBindingCheckbox(){
    for(let i = 0; i < this.developers.length; i++) {
      if (this.paramsValue) {
        const isSame = this.projectData.developers.filter((devs) =>{
          return devs.id === this.developers[i].devid;
        });
        if(isSame.length > 0){
          const devDataObject = new DevCheckboxModel(
            this.developers[i].devid,
            this.developers[i].devname,
            true
          );
          this.isDeveloperChecked.push(devDataObject);
        }else{
          const devDataObject = new DevCheckboxModel(
            this.developers[i].devid,
            this.developers[i].devname,
            false
          );
          this.isDeveloperChecked.push(devDataObject);
        }
      }else{
        const devDataObject = new DevCheckboxModel(
          this.developers[i].devid,
          this.developers[i].devname,
          false
        );
        this.isDeveloperChecked.push(devDataObject);
      }
    }
  }
  onSetPorjectDate(edit: boolean = false){
      if(edit){
        this.updatePorjectStartDate = this.projectData.projectstart;
        this.updatePorjectEndDate = this.projectData.projectdeadline;
        const tmpStart = this.updatePorjectStartDate.split('/');
        const tmpEnd = this.updatePorjectEndDate.split('/');
        this.startDate = { date: { 
          year: +tmpStart[0],
          month: +tmpStart[1],
          day: +tmpStart[2]
        } };
        this.endDate = { date: { 
          year: +tmpEnd[0],
          month: +tmpEnd[1],
          day: +tmpEnd[2]
        } };
      }else{
        this.startDate = { date: { year: this.thisYear, month: this.thisMonth, day: this.thisDay } };
        this.endDate = { date: { year: this.thisYear, month: this.thisMonth, day: this.thisDay } };
      }
  }
  onSubmitForm(){
    if(this.projectForm.valid){
      this.projectObject = this.projectForm.value;
      this.onSubmitDevs(this.selectedOptions());
      if(this.paramsValue){
        const projectObject = new ProjectModel(
          this.projectIdValue,
          this.projectForm.get('projectname').value,
          this.projectForm.get('projectdesc').value,
          this.projectForm.get('projectstart').value.date.year
          + '/' + this.projectForm.get('projectstart').value.date.month
          + '/' + this.projectForm.get('projectstart').value.date.day,
          this.projectForm.get('projectdeadline').value.date.year
          + '/' + this.projectForm.get('projectdeadline').value.date.month
          + '/' + this.projectForm.get('projectdeadline').value.date.day,
          this.projectForm.get('projectbonus').value,
          this.projectObject.developers,
          this.projectData.projectend,
          this.projectData.projectstate
        );
        const confirmVal = new AlertModel(
          true,
          'Confirm...',
          'Are you sure you want to update this project?',
          false,
          2,
          1,
          projectObject
        );
        this.confirm.onAlertCalled.next(confirmVal);
      }else{
        const confirmVal = new AlertModel(
          true,
          'Confirm...',
          'Are you sure you want to save this project?',
          false,
          1,
          1,
          this.projectObject
        );
        this.confirm.onAlertCalled.next(confirmVal);
      }
    }else{
      this.confirm.onAlertCalled.next(new AlertModel(true,'Alert...','Please Check the Form...', true));
    }
  }
  projectStartValidation(control: FormControl): Promise<any> | Observable<any> {
    const fromDate = new Date(this.thisYear + '-' + this.thisMonth + '-' + this.thisDay);
    const promise = new Promise<any>((resolve, reject)=>{
      setTimeout(()=>{
        const newDate = new Date(control.value.date.year + '-' + control.value.date.month + '-' + control.value.date.day);
        if (this.paramsValue) {
          resolve(null);
        } else{
          if(newDate < fromDate) {
            resolve({'startDateIsInvalid': true});
          }else{
            resolve(null);
          }
        }
      }, 400);
    });
    return promise;
  }
  projectEndValidation(control: FormControl): Promise<any> | Observable<any> {
    const defaultFromDate = new Date(this.thisYear + '-' + this.thisMonth + '-' + this.thisDay);
    const updateFromDate = new Date(
      this.projectForm.get('projectstart').value.date.year
      + '-' + this.projectForm.get('projectstart').value.date.month
      + '-' + this.projectForm.get('projectstart').value.date.day
    );
    const fromDate = this.paramsValue ? updateFromDate : defaultFromDate;
    const promise = new Promise<any>((resolve, reject)=>{
      setTimeout(()=>{
        const newDate = new Date(control.value.date.year + '-' + control.value.date.month + '-' + control.value.date.day);
        if(newDate <= fromDate) {
          resolve({'startDateIsInvalid': true});
        }else{
          resolve(null);
        }
      }, 400);
    });
    return promise;
  }
  onSubmitDevs(dev: any) {
    this.projectObject.developers = [];
    for(let i = 0; i < dev.length; i++) {
      if (dev[i]) {
        this.projectObject.developers.push({id: dev[i]});
      }
    }
  }
  onFormCLear(){
    this.projectForm.reset();
    this.isDeveloperChecked = [];
    this.onBindingCheckbox();
    this.onSetPorjectDate();
  }
}
