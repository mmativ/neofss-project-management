export class DevCheckboxModel {
    constructor(
        public value: any,
        public devname: string,
        public checked: boolean,
    ) {}
}