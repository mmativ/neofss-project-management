import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'convertToStatus'})
export class StatusPipes implements PipeTransform{
    transform(value: number){
        if(value == 0){
            return 'Not Started';
        }else if (value == 1) {
            return 'On Going';
        }else if (value == 2) {
            return 'Almost Done';
        }else if (value == 3) {
            return 'Success';
        }else if (value == 4) {
            return 'Failed';
        }else if (value == 5 || value >= 5) {
            return 'Pending';
        }
    }
}