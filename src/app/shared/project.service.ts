import { ProjectModel } from './project.model';

export class ProjectService {
    private projects: ProjectModel[] = [
        new ProjectModel(
            't7g4cw',
            'Coone',
            'PSD to HTML, responsive 1920 - 320px',
            '2017/06/1',
            '2017/06/23',
            1000,
            [
                {id: '5fPS5cnc'},
                {id: 'MQ45F2uq'}
            ]
        ),
        new ProjectModel(
            't7g54l',
            'Fanball',
            'PSD to HTML, responsive 1920 - 320px',
            '2017/06/1',
            '2017/06/28',
            1000,
            [
                {id: '5fPS5cnc'},
                {id: 'MQ45F2uq'}
            ]
        ),
        new ProjectModel(
            'n4ru3u',
            'The Marinus',
            'PSD to HTML, responsive 1920 - 320px',
            '2017/06/22',
            '2017/06/30',
            1500,
            [
                {id: 'MQ45F2uq'},
                {id: 'Cyqd6TKh'}
            ]
        ),
        new ProjectModel(
            '3szu08',
            'Handbook',
            'PSD to HTML, responsive 1920 - 320px',
            '2017/06/12',
            '2017/06/16',
            1500,
            [
                {id: 'MQ45F2uq'},
                {id: 'Cyqd6TKh'}
            ],
            '2017/07/12',
            3
        ),
        new ProjectModel(
            'dascjp',
            'Total Sites',
            'PSD to HTML, responsive 1920 - 320px',
            '2017/06/5',
            '2017/06/20',
            1500,
            [
                {id: 'MQ45F2uq'},
                {id: 'Cyqd6TKh'}
            ]
        ),
        new ProjectModel(
            'dasc2p',
            'Juvair',
            'PSD to HTML, responsive 1920 - 320px',
            '2017/07/7',
            '2017/07/24',
            1500,
            [
                {id: 'MQ45F2uq'},
                {id: 'Cyqd6TKh'}
            ]
        )
    ];
    projectIndexFinder(projectId: string = null){
        const projectTarget = this.projects.map(function(x) {return x.projectid; }).indexOf(projectId);
        return projectTarget;
    }
    getDevelopersProject(devId: string = null) {
        const devProjectsObject = [];
        for (let project of this.projects){
            for (let dev of project.developers){
                if(dev.id == devId){
                    devProjectsObject.push(project);
                }
            }
        }
        return devProjectsObject;
    }

    getProjects() {
        return this.projects.slice();
    }
    getSingleProject(projectId){
        return this.projects.filter(x => x.projectid === projectId)[0];
    }
    onAddProject(projectObject: ProjectModel){
        this.projects.push(projectObject);
    }
    onUpdateProject(projectObject: ProjectModel) {
        const index = this.projectIndexFinder(projectObject.projectid);
        this.projects[index] = projectObject;
    }
    onDeleteProject(projectId: string = null){
        const index = this.projectIndexFinder(projectId);
        this.projects.splice(index, 1);
    }
}