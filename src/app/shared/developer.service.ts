import { DeveloperModel } from './developers.model';
import { Subject } from 'rxjs/Subject';

export class DeveloperService{
    devDatasSync = new Subject<DeveloperModel[]>();
    private devDatas: DeveloperModel[] = [
        new DeveloperModel(
            '5fPS5cnc',
            'Mart LLoyd Mativo',
            'Publisher',
            'mart.mativo@gmail.com',
            '09215343735',
            'Admin123'
        ),
        new DeveloperModel(
            'MQ45F2uq',
            'John Doe',
            'Publisher',
            'johndoe@gmail.com',
            '09211111111',
            'Admin123'
        ),
        new DeveloperModel(
            'Cyqd6TKh',
            'Jane Deng',
            'Publisher',
            'janedeng@gmail.com',
            '09211111111',
            'Admin123'
        )
    ];
    projectIndexFinder(devId: string = null){
        const devTarget = this.devDatas.map(function(x) {return x.devid; }).indexOf(devId);
        return devTarget;
    }
    onGetDevelopers(){
        return this.devDatas.slice();
    }
    onGetSingleDevelopers(devId){
        return this.devDatas.filter(x => x.devid === devId)[0];
    }
    onAddDeveloper(devData: DeveloperModel) {
        this.devDatas.push(devData);
        this.devDatasSync.next(this.devDatas.slice());
    }
    onUpdateDeveloper(devModel: DeveloperModel){
        const index = this.projectIndexFinder(devModel.devid);
        this.devDatas[index] = devModel;
        this.devDatasSync.next(this.devDatas.slice());
    }
}