export class ProjectLogicModel {
    constructor(
        public projectid: string,
        public projectname: string,
        public projectdesc: string,
        public projectstart: string,
        public projectdeadline: string,
        public projectdaysleft: number,
        public projectstatus: number,
        public projectbonus: number,
        public developers: [{}],
        public projectend: any = null,
        public projectstate: number = null,
    ) {}
}