export class ProjectModel {
    constructor(
        public projectid: string,
        public projectname: string,
        public projectdesc: string,
        public projectstart: string,
        public projectdeadline: string,
        public projectbonus: number,
        public developers: any,
        public projectend: any = null,
        public projectstate: any = null,
    ) {}
}