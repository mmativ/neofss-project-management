import { Injectable } from '@angular/core';
import { ProjectLogicModel } from './project-logic.model';
import { ProjectService } from './project.service';

@Injectable()
export class ProjectLogicService{
    private projectDatas: ProjectLogicModel[] = [];
    private projectDetails: ProjectLogicModel[] = [];

    constructor(private projectsvc: ProjectService){
        this.projectDatas = [];
    }
    onDevGetListOfProjects(devId: string = null){
        this.projectDatas = [];
        const devProjectData = this.listProjectLogic(this.projectsvc.getDevelopersProject(devId));
        return devProjectData;
    }
    onGetProjectDatas(){
        this.projectDatas = [];
        const projectDatas = this.listProjectLogic(this.projectsvc.getProjects());
        return projectDatas;
    }
    onGetProjectDetails(projectId: any){
        this.projectDetails = [];
        const singleProjectData = this.singleProjectLogic(this.projectsvc.getSingleProject(projectId));
        return singleProjectData;
    }
    onGetProjectFiltered(value){
        const projectsFiltered = this.projectDatas.filter(x => x.projectstatus === value);
        return projectsFiltered;
    }
    getWorkingDays(start_date: any , end_date: any): number {
        let fromDate = new Date(start_date);
        let toDate = new Date(end_date);
        let workingdays = 0;
        while (fromDate <= toDate) {
            let weekday = fromDate.getDay();
            if (weekday > 0 && weekday < 6) {
            workingdays++;
            }
            fromDate = new Date(fromDate.getTime() + (60 * 60 * 24 * 1000));
        }
        return workingdays;
    }
    checkStatus(daysleft: number, projectstart: any, iscurrentdaylogic: boolean){
        let statusResult;
        /* 
        statusResult value..
        0 Not Started 
        1 On going
        2 Almost done
        3 Success
        4 failed
        5 pending
        */
        if(iscurrentdaylogic) {
            if(daysleft > 0 && daysleft <= 5){
                statusResult = 2;
            }else if(daysleft >= 6){
                statusResult = 1;
            }else{
                statusResult = 5;
            }
        }else{
            statusResult = 0;
        }
        return statusResult;
    }

    listProjectLogic(datas: any) {
        datas.forEach((item) => {
            let currentDay = this.getCurrentDate();
            const projStart = item.projectstart;
            const projEnd = item.projectdeadline;
            let whatLogic = this.isCurrentDayLogic(projStart);
            let daysLefts;
            // // check what logic for days left
            if(!whatLogic){
                daysLefts = this.getWorkingDays(projStart, projEnd);
            }else{
                daysLefts = this.getWorkingDays(currentDay, projEnd);
            }

            // check logic for status
            let status;
            if(!item.projectstate){
                status = this.checkStatus(daysLefts, projStart, whatLogic);
            }else{
                status = item.projectstate;
            }
            // console.log('======================');
            // console.log(status, 'status');
            // console.log(daysLefts, 'days left');
            // console.log('======================');
            this.projectDatas.push(
                new ProjectLogicModel(
                    item.projectid,
                    item.projectname,
                    item.projectdesc,
                    item.projectstart,
                    item.projectdeadline,
                    daysLefts,
                    status,
                    item.projectbonus,
                    item.developers,
                    item.projectend,
                    item.projectstate)
            );
        });
        return this.projectDatas;
    }
    singleProjectLogic(datas: any){
        let currentDay = this.getCurrentDate();
        const projStart = datas.projectstart;
        const projEnd = datas.projectdeadline;
        let whatLogic = this.isCurrentDayLogic(projStart);
        let daysLefts;
        // // check what logic for days left
        if(!whatLogic){
            daysLefts = this.getWorkingDays(projStart, projEnd);
        }else{
            daysLefts = this.getWorkingDays(currentDay, projEnd);
        }
        // check logic for status
        let status;
        if(!datas.projectstate){
            status = this.checkStatus(daysLefts, projStart, whatLogic);
        }else{
            status = datas.projectstate;
        }
        this.projectDetails.push(
            new ProjectLogicModel(
                datas.projectid,
                datas.projectname,
                datas.projectdesc,
                datas.projectstart,
                datas.projectdeadline,
                daysLefts,
                status,
                datas.projectbonus,
                datas.developers,
                datas.projectend,
                datas.projectstate)
        );
        return this.projectDetails;
    }
    isCurrentDayLogic(start): boolean{
        let currentDate: any = new Date();
        let projStart: any = new Date(start);
        if(currentDate.getTime() > projStart.getTime()) {
                return true;
        }else{
                return false;
        }
    }
    getCurrentDate(): any{
        let today: any = new Date();
        let dd: any = today.getDate();
        let mm: any = today.getMonth()+1;
        let yyyy: any = today.getFullYear();
        if(dd<10) {
            dd='0'+dd
        } 
        if(mm<10) {
            mm='0'+mm
        } 
        today = yyyy+'/'+mm+'/'+dd;
        return today;
    }
    getStatusCount(statusNum){
        const statusCount = this.projectDatas.filter(x => x.projectstatus === statusNum).length;
        return statusCount;
    }
}