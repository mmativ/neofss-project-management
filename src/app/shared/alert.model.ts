export class AlertModel{
    public status: boolean;
    public alertheader: string;
    public alertmessage: string;
    public isalert: boolean;
    public operationtype: number;
    public operationsubject: string;
    public object: any;
    constructor(
        private alertStatus: boolean = false,
        private alertHeader: string = 'Alert',
        private alertMessage: string = 'Alert Message',
        private isAlert: boolean = true,
        private operationType: any = null,
        private operationSubject: any = null,
        private dataObject: any = null
    ){
        this.status = alertStatus;
        this.alertheader = alertHeader;
        this.alertmessage = alertMessage;
        this.isalert = isAlert;
        this.operationtype = operationType;
        this.operationsubject = operationSubject;
        this.object = dataObject;
    }
}