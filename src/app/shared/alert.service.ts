import { EventEmitter } from '@angular/core';
import { AlertModel } from './alert.model';
import { Subject } from 'rxjs/Subject';

export class AlertService{
    onAlertCalled =  new Subject<AlertModel>();
    onALertResponse = new Subject<boolean>();
}