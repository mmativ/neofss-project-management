export class DeveloperModel{
    constructor(
        public devid: any,
        public devname: any,
        public devposition: any,
        public devemail: any,
        public devcontact: any,
        public devpwd: any,
    ) {}
}