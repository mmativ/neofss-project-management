import { NeofssProjectManagementPage } from './app.po';

describe('neofss-project-management App', () => {
  let page: NeofssProjectManagementPage;

  beforeEach(() => {
    page = new NeofssProjectManagementPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
